package br.com.zup.Entities;

import java.util.ArrayList;
import java.util.List;

public class Imobiliaria {

    private static List<Imovel> imoveis = new ArrayList<>();

    // Método para cadastrar um imóvel em uma lista de imóveis
    public static void cadastrarImovel(double valor, Endereco endereco, List<Morador> moradores, Funcionario funcionario) {
        imoveis.add(new Imovel(valor,endereco,moradores,funcionario));
        IO.exibir("\nImóvel cadastrado com sucesso!");
    }

    // Método responsável por exibir os imóveis da lista de imóveis
    public static void mostrarImoveis() throws Exception {
        // Verificando se a lista está vazia
        if(imoveis.size() == 0){
            throw new Exception("\nNenhum imóvel encontrado!");
        } else {
            for(Imovel imovel : imoveis) {
                System.out.println(imovel);
            }
        }
    }


    // Método responsável por excluir o morador de um imóvel específico
    public static void excluirMorador(String cep, int num, String cpf) throws Exception {
        // Verificando se a lista está vazia
        if(imoveis.size() == 0) {
            throw new Exception("\nNenhum Imóvel Cadastrado!");
        } else {
            // Percorre a lista de imóveis
            for(Imovel i : imoveis){
                // Verifica se o cep e o número são iguais aos que foram passados por parâmetro
                if(i.getEndereco().getCep().equals(cep) && i.getEndereco().getNumRua() == num){
                    // Remove o morador da lista de imóveis se o cpf do m(morador) for igual
                    // ao que foi passado por parâmetro
                    if(i.getMoradores().removeIf(m -> m.getCpf().equals(cpf))){
                        IO.exibir("\nMorador excluído com sucesso!");
                    } else {
                        throw new Exception("\nMorador não localizado!");
                    }
                }
            }
        }
    }

    public static Morador verificaCpf(Morador morador) throws Exception {
        for(Imovel imv : imoveis){
            for(Morador mrd : imv.getMoradores()){
                if(mrd.getCpf().equals(morador.getCpf())){
                    throw new Exception("CPF já cadastrado!");
                }
            }
        }
        return morador;
    }

}
