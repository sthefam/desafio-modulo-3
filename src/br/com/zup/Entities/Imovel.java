package br.com.zup.Entities;

import java.util.ArrayList;
import java.util.List;

public class Imovel {

    // Atributos
    private double valor;
    private Endereco endereco;
    private List<Morador> moradores = new ArrayList<>();
    private Funcionario funcionario;

    // Construtor vazio
    public Imovel(){

    }

    // Construtor com parâmetros
    public Imovel(double valor, Endereco endereco, List<Morador> moradores, Funcionario funcionario) {
        this.valor = valor;
        this.endereco = endereco;
        this.moradores = moradores;
        this.funcionario = funcionario;
    }

    // Getters e Setters
    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Morador> getMoradores() {
        return moradores;
    }

    public void setMoradores(List<Morador> moradores) {
        this.moradores = moradores;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    // Formatando a saída dos dados
    @Override
    public String toString() {
        StringBuilder objSb = new StringBuilder();
        objSb.append("\nImóvel\n");
        objSb.append("\nValor do aluguel: R$ ").append(valor);
        objSb.append("\n\nEndereço \n").append(endereco);
        objSb.append("\n\nMoradore(s) \n\n").append(moradores);
        objSb.append("\n\nFuncionário\n").append(funcionario);
        return objSb.toString();
    }
}
