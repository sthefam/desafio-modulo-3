package br.com.zup.Entities;

public class Morador extends Pessoa {

    // Atributos
    private String numContrato;

    // Construtor vazio
    public Morador(){

    }

    // Construtor com parâmetros
    public Morador(String nome, String cpf, String telefone, String numContrato) {
        super(nome, cpf, telefone);
        this.numContrato = numContrato;
    }

    // Getters e Setters
    public String getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(String numContrato) {
        this.numContrato = numContrato;
    }

    // Formatando a saída dos dados
    @Override
    public String toString() {
        StringBuilder objSb = new StringBuilder();
        objSb.append("\nNome: ").append(getNome());
        objSb.append("\nCPF: ").append(getCpf());
        objSb.append("\nTelefone: ").append(getTelefone());
        objSb.append("\nContrato: ").append(numContrato);
        return objSb.toString();
    }
}
