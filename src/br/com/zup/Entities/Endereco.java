package br.com.zup.Entities;

public class Endereco {

    // Atributos
    private String cep;
    private String rua;
    private int numRua;

    // Construtor vazio
    public Endereco(){

    }

    // Construtor com parâmetros
    public Endereco(String cep, String rua, int numRua) {
        this.cep = cep;
        this.rua = rua;
        this.numRua = numRua;
    }

    // Getters e Setters
    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getNumRua() {
        return numRua;
    }

    public void setNumRua(int numRua) {
        this.numRua = numRua;
    }

    // Formatando a saída dos dados
    @Override
    public String toString() {
        StringBuilder objSb = new StringBuilder();
        objSb.append("\nCEP: ").append(cep);
        objSb.append("\nRua: ").append(rua);
        objSb.append(", ").append(numRua);
        return objSb.toString();
    }
}
