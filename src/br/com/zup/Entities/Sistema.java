package br.com.zup.Entities;

import java.util.ArrayList;
import java.util.List;

public class Sistema {

    // Método responsável por exibir o menu
    public static void menu(){
        IO.exibir("\nSFS - Imobiliária\n");
        IO.exibir("1 - Cadastrar Imóveis");
        IO.exibir("2 - Excluir Morador");
        IO.exibir("3 - Exibir Imóveis");
        IO.exibir("4 - Sair");
    }

    // Método responsável por capturar a opção escolhida pelo usuário
    public static void executar() {
        String op = null;
        do{
            menu();
            op = IO.ler().next();
            try {
                switch (op){
                    case "1" -> cadastrarImovel();
                    case "2" -> excluirMorador();
                    case "3" -> Imobiliaria.mostrarImoveis();
                    case "4" -> System.exit(0);
                    default -> op = "4";
                }
            }catch (Exception e){
                IO.exibir(e.getMessage());
                executar();
            }
        }while (!op.equals("4"));
    }

    // Método responsável por capturar os dados do imóvel e funcioário
    public static void cadastrarImovel() throws Exception {
        IO.exibir("\nDados do Imóvel\n");
        IO.exibir("Digite o valor do aluguel: ");
        double valor = IO.ler().nextDouble();
        IO.exibir("Digite o CEP do imóvel: ");
        String cep = IO.ler().next();
        IO.exibir("Digite o nome da rua: ");
        String rua = IO.ler().nextLine();
        IO.exibir("Digite o número do imóvel: ");
        int numRua = IO.ler().nextInt();
        IO.exibir("\nDados do Funcionário Responsável\n");
        IO.exibir("Digite o nome do funcionário responsável: ");
        String nome = IO.ler().nextLine();
        IO.exibir("Digite o CPF do funcionário responsável: ");
        String cpf = IO.ler().next();
        IO.exibir("Digite o telefone do funcionário responsável: ");
        String telefone = IO.ler().next();
        IO.exibir("Digite a matricula do funcionário responsável: ");
        String matricula = IO.ler().next();
        IO.exibir("Digite o salário do funcionário responsável: ");
        double salario = IO.ler().nextDouble();
        Imobiliaria.cadastrarImovel(valor,new Endereco(cep,rua,numRua),cadastrarMoradores(),new Funcionario(nome,cpf,telefone,matricula,salario));
        IO.exibir("\nEscolha uma opção abaixo\n");
        IO.exibir("1 - Cadastrar outro Imóvel");
        IO.exibir("2 - Voltar ao Menu Inicial");
        String op = IO.ler().next();
        if(op.equals("1")){
            cadastrarImovel();
        }
    }

    // Método responsável por capturar os dados do morador e retornar para o método
    // cadastrar imóvel para ser processado na classe imobiliária
    public static List<Morador> cadastrarMoradores() throws Exception {
        List<Morador> moradores = new ArrayList<>();
        String op = "";
        do {
            IO.exibir("\nDados do Morador\n");
            IO.exibir("Digite o nome do morador: ");
            String nome = IO.ler().nextLine();
            IO.exibir("Digite o CPF do morador: ");
            String cpf = IO.ler().next();
            IO.exibir("Digite o telefone do morador: ");
            String telefone = IO.ler().next();
            IO.exibir("Digite o número do contrato do morador: ");
            String numContrato = IO.ler().next();
            for(Morador m : moradores){
                if(m.getCpf().equals(cpf)){
                    throw new Exception("\nMorador já cadastrado!");
                }
            }
            moradores.add(Imobiliaria.verificaCpf(new Morador(nome,cpf,telefone,numContrato)));
            IO.exibir("\nEscolha uma opção abaixo\n");
            IO.exibir("1 - Cadastrar outro Morador");
            IO.exibir("2 - Voltar");
            op = IO.ler().next();
        } while (op.equals("1"));
        return moradores;
    }

    // Método responsável por capturar os dados do morador a ser excluído
    public static void excluirMorador() throws Exception {
        IO.exibir("\nExcluir Morador\n");
        IO.exibir("Digite o CEP do imóvel: ");
        String cep = IO.ler().next();
        IO.exibir("Digite o número do imóvel: ");
        int num = IO.ler().nextInt();
        IO.exibir("Digite o CPF do morador: ");
        String cpf = IO.ler().next();
        Imobiliaria.excluirMorador(cep,num,cpf);
    }

}
