package br.com.zup.Entities;

public class Funcionario extends Pessoa {

    // Atributos
    private String matricula;
    private double salario;

    // Construtor vazio
    public Funcionario(){

    }

    // Construtor com parâmetros
    public Funcionario(String nome, String cpf, String telefone, String matricula, double salario) {
        super(nome, cpf, telefone);
        this.matricula = matricula;
        this.salario = salario;
    }

    // Getters e Setters
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    // Formantando a saída dos dados
    @Override
    public String toString() {
        StringBuilder objSb = new StringBuilder();
        objSb.append("\nNome: ").append(getNome());
        objSb.append("\nCPF: ").append(getCpf());
        objSb.append("\nTelefone: ").append(getTelefone());
        objSb.append("\nMatricula: ").append(matricula);
        objSb.append("\nSalário: R$ ").append(salario);
        return objSb.toString();
    }
}
