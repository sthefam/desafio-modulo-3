package br.com.zup.Entities;

import java.util.Scanner;

public class IO {

    // Método responsável por exibir um texto
    public static void exibir(String texto){
        System.out.println(texto);
    }

    // Método responsável por capturar os dados digitados pelo usuário
    public static Scanner ler(){
        return new Scanner(System.in);
    }

}
